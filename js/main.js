const channelsBlock = document.querySelector('.channels');
const genresBlock = document.querySelector('.genres');
const tvshowsBlock = document.querySelector('.tvshows');

genresBlock.addEventListener('change', filterChannels);
channelsBlock.addEventListener('click', loadTvshows);

let allChannels = [];

allData$.then(([data, genres]) => {
    allChannels = data.channels;
    renderItems(channelsBlock, allChannels, getChannelCard);
    const allGenres = addAllGenres(genres);
    renderGenres(allGenres);
})
    .catch(err => console.log(err));

function addAllGenres(genres) {
    const allGenres = { id: 1, name: 'All genres' };
    const newGenres = [allGenres, ...genres];
    return newGenres;
}

function getChannelCard({ logo, name, channel_id }) {
    const channelCard = getTag('div', 'channel');
    channelCard.id = channel_id;
    const logoTag = getTag('img', 'logo', logo);
    const nameTag = getTag('span', 'ch-name', name);
    channelCard.append(logoTag, nameTag);

    return channelCard;
}

function renderGenres(genres) {
    genresBlock.innerHTML = '';
    const genreTags = genres.map(item => {
        const option = getTag('option', 'option', item.name);
        option.value = item.id;
        return option;
    });

    genresBlock.append(...genreTags);
}

function filterChannels(event) {
    const genreId = Number(event.target.value);
    if (genreId === 1) {
        renderItems(channelsBlock, allChannels, getChannelCard);
    } else {
        const filteredChannels = allChannels.filter(item => item.genres.includes(genreId));
        renderItems(channelsBlock, filteredChannels, getChannelCard);
    }
}

function loadTvshows(event) {
    const channelId = event.target.className.includes('channel') ? event.target.id : event.target.parentNode.id;
    const currentDate = moment().format('YYYY-MM-DD');
    getTvshows(channelId, currentDate, currentDate)
        .then(data => renderItems(tvshowsBlock, data.tvshows.items, createShowCard));
}

function createShowCard(tvshow) {
    const startTime = moment.unix(tvshow.start).format('HH:mm');
    const endTime = moment.unix(tvshow.stop).format('HH:mm');
    const currentTime = moment().unix();
    const isCurrent = currentTime >= tvshow.start && currentTime < tvshow.stop;
    const cardClass = isCurrent ? 'show current-show' : 'show';
    const card = getTag('div', cardClass);
    const name = getTag('span', 'show-name', tvshow.title);
    const info = getTag('div', 'show-info');
    const start = getTag('span', 'time', startTime);
    const stop = getTag('span', 'time', endTime);

    info.append(start, stop);
    card.append(name, info);

    return card;
}

function renderItems(renderBlock, items, creator) {
    renderBlock.innerHTML = '';
    const tags = items.map(item => creator(item));
    renderBlock.append(...tags);
}
