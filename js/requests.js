const BASE_URL = 'https://api.persik.by/v2/';

const channels$ = fetch(`${BASE_URL}content/channels`, { method: 'GET' })
    .then(response => response.json());

const genres$ = fetch(`${BASE_URL}categories/channel`, { method: 'GET' })
    .then(response => response.json());
const allData$ = Promise.all([channels$, genres$]);

function getTvshows(channelId, from, to) {
    return fetch(`${BASE_URL}epg/tvshows?limit=1000&channels[]=${channelId}&from=${from}&to=${to}`, { method: 'GET' })
            .then(response => response.json());
}